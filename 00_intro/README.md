---

# Ansible


.footer: Created By Alex M. Schapelle, VAioLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Ansible ?
- Who needs Ansible ?
- How Ansible works ?
- How to manage Ansible in various environments and scenarios?
- What is Yaml ?
- What are playbooks?
- How can use write and use Playbooks for our use?


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of Ansible
- For junior/senior developers/operations who need knowledge in automation Infra-As-A-Code.
- Experienced ops who need refresher


---

# Course Topics

- Intro
- Env Setup
- Fundamental Commands
- Yaml Basics
- Playbook Structure
- Advance Execution
- Roles
- TroubleShooting.


---
# About Me
<img src="99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- over 12 years of IT industry Experience.
- fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - between each semester, I tried to take IT course at various places.
        - yes, one of them was A+.
        - yes, one of them was cisco.
        - yes, one of them was RedHat course.
        - yes, one of them was LPIC1 and Shell scripting.
        - no, others i learned alone.
        - no, not maintaining debian packages any more.
---

# About Me (cont.)
- over 7 years of sysadmin:
    - shell scripting fanatic
    - python developer
    - js admirer
    - golang fallen
    - rust fan
- 5 years of working with devops
    - git supporter
    - vagrant enthusiast
    - ansible consultant
    - container believer
    - k8s user

you can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# History

The term "ansible" was coined by Ursula K. Le Guin in her 1966 novel Rocannon's World, and refers to fictional `instantaneous communication systems`.
The Ansible tool was developed by Michael DeHaan, the author of the provisioning server application Cobbler and co-author of the Fedora Unified Network Controller (Func) framework for remote administration.
Ansible, Inc. (originally AnsibleWorks, Inc.) was the company founded in 2013 by Michael DeHaan, Timothy Gerla, and Saïd Ziouani to commercially support and sponsor Ansible. Red Hat acquired Ansible in October 2015.
Ansible is included as part of the Fedora distribution of Linux, owned by Red Hat, and is also available for Red Hat Enterprise Linux, CentOS, openSUSE, SUSE Linux Enterprise, Debian, Ubuntu, Scientific Linux, and Oracle Linux via Extra Packages for Enterprise Linux (EPEL), as well as for other operating systems

---

## What is Ansible?
Ansible is an open-source software provisioning, configuration management, and application-deployment tool enabling infrastructure as code.

## Why we need it?

Lets assume that you have a couple of remote instances running some services. Due to some issue or upgrade in those services you might need to alter the configurations in those remote instances. What you would have to do is make those changes in each of those instances manually. The importance of a configuration management system comes in handy in a situation like this. Do the configuration changes in a master instance and it will make sure that all other instances would have the proper changes. Ansible is a such configuration management tool. Ansible connects with its other instances using SSH. Therefore there is no concept such as an agent when using Ansible

---
# Ansible (cont.)


## Why not scripts: Bash/Python/Ruby/JavaScript/Go

- Lets discuss

---

# Ansible (cont.)
### What are prerequisites ?

Ansible requires Python to be installed on all `managing` machines, including pip package manager along with configuration-management software and its dependent packages. 
Managed network devices require no extra dependencies and are agentless. We can sum it up with:

- Ssh server
- Python3, with pip3

## How Ansible works ?

### inventory file

- The Inventory is a description of the nodes that can be accessed by Ansible. 
- The Inventory is described by a configuration file, in INI or YAML format, whose default location is in /etc/ansible/hosts. 
- The configuration file lists either the IP address or hostname of each node that is accessible by Ansible. In addition, nodes can be assigned to groups

---

# Ansible (cont.)
## How Ansible works ?
### ansible modules

- Modules are mostly standalone and can be written in a standard scripting language (such as Python, Perl, Ruby, Bash, etc.). 
- One of the guiding properties of modules is idempotency, which means that even if an operation is repeated multiple times (e.g., upon recovery from an outage), it will always place the system into the same state

---

# Ansible (cont.)
## How Ansible works ?
### ansible playbooks

- Playbooks are YAML files that express configurations, deployment, and orchestration in Ansible, and allow Ansible to perform operations on managed nodes. 
- Each Playbook maps a group of hosts to a set of roles. Each role is represented by calls to Ansible tasks

---

# Ansible (cont.)
## How Ansible works ?
### ansible roles
- Each Playbook maps a group of hosts to a set of roles. Each role is represented by calls to Ansible tasks.

